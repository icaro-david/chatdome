ChatDome é uma aplicação de chat em tempo real criada por Ícaro David usando React, Node, Express e Socket.IO.

Para usar basta conectar-se ao site <a href="https://chatdome.netlify.app">ChatDome</a> inserir seu nome de usuário, (o nome que ficará visível para os outros usuários na sala) e escolher um nome para a sua sala. 

Envie o nome da sala que você colocou para os seus amigos e divirtam-se conversando.

OBS.: O servidor não irá carregar e a página não retornará nenhuma resposta nos seguintes casos: alguém ja está presente na sala com o mesmo nome de usuário que você está tentando usar, nesse caso escolha outro nome de usuário;

Atenciosamente,

Ícaro David ;)
